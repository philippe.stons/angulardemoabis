import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponentComponent } from './components/home-component/home-component.component';
import { TestComponentComponent } from './components/test-component/test-component.component';
import { NavComponent } from './components/nav/nav.component';
import {ExampleModule} from "./modules/example/example.module";
import {DemoModule} from "./modules/demo/demo.module";
import {FormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponentComponent,
    TestComponentComponent,
    NavComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DemoModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
