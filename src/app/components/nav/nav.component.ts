import {Component, Input, OnInit} from '@angular/core';

class NavBarItem {
  title: string;
  url: string;
}

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  @Input() title: string;
  items: Array<NavBarItem>;

  constructor() { }

  ngOnInit(): void {
    this.items = [
      { title: 'Home', url: '/home' },
      { title: 'Test', url: '/test' },
      { title: 'Demo 1', url: '/demo/demo1' },
      { title: 'Demo 2', url: '/demo/demo2' },
      { title: 'Demo 3', url: '/demo/demo3' },
      { title: 'Demo 4', url: '/demo/demo4' },
      { title: 'Demo 5', url: '/demo/demo5' },
      // { title: 'Demo User', url: '/demo/demouser' },
      // { title: 'Demo Admin', url: '/demo/demoadmin' },
      { title: 'Demo Admin', url: '/demo/demo6/admin' },
      { title: 'Demo User', url: '/demo/demo6/user' },
      { title: 'Demo Param', url: '/demo/demo7/1' },
      { title: 'Demo Resolver', url: '/demo/demo8/1' },
      { title: 'Demo 9', url: '/demo/demo9' },
      { title: 'Demo 10', url: '/demo/demo10' },
      { title: 'Exercice 1', url: '/exercices/ex1' },
      { title: 'Exercice 2', url: '/exercices/ex2' },
      { title: 'Exercice 3', url: '/exercices/ex3' },
      { title: 'Fan add', url: '/exercices/ex4/add' },
      { title: 'Fan list', url: '/exercices/ex4/list' },
    ]
  }

  myMethod()
  {
    console.log("HELLO!")
  }
}
