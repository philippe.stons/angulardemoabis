import {environment} from "../../environments/environment";

export class Logger
{
  static log(msg: string, data?: any)
  {
    if(environment.production)
    {
      return;
    }

    console.log('> ' + msg);

    if(data !== undefined)
    {
      if(Array.isArray(data) && console.table !== undefined)
      {
        console.table(data.slice(0,50));
      } else
      {
        console.log(data);
      }
    }
  }
}
