import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponentComponent} from "./components/home-component/home-component.component";
import {TestComponentComponent} from "./components/test-component/test-component.component";

const routes: Routes = [
  { path: 'home', component: HomeComponentComponent },
  { path: 'test', component: TestComponentComponent },
  { path: 'examples', loadChildren: () => import('./modules/example/example.module')
      .then((m) => m.ExampleModule)
  },
  { path: 'exercices', loadChildren: () => import('./modules/exercices/exercices.module')
      .then((m) => m.ExercicesModule)
  },
  { path: 'demo', loadChildren: () => import('./modules/demo/demo.module')
      .then((m) => m.DemoModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
