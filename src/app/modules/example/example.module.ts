import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Example1Component } from './components/example1/example1.component';
import {ExampleRoutingModule} from "./example.routing.module";
import {Routes} from "@angular/router";

const routes: Routes = [
  { path: 'ex1', component: Example1Component },
]

@NgModule({
  declarations: [
    Example1Component
  ],
  imports: [
    CommonModule,
    ExampleRoutingModule,
  ]
})
export class ExampleModule { }
