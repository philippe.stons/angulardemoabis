import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DemoRoutingModule} from "./demo-routing.module";
import { Demo1Component } from './components/demo1/demo1.component';
import {SharedModule} from "../shared/shared.module";
import { Demo2Component } from './components/demo2/demo2.component';
import { Demo3Component } from './components/demo3/demo3.component';
import { ChildComponent } from './components/demo3/child/child.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { Demo4Component } from './components/demo4/demo4.component';
import { Demo5Component } from './components/demo5/demo5.component';
import { Demo6Component } from './components/demo6/demo6.component';
import { AdminComponent } from './components/demo6/admin/admin.component';
import { UserComponent } from './components/demo6/user/user.component';
import { Demo7Component } from './components/demo7/demo7.component';
import { Demo8Component } from './components/demo8/demo8.component';



@NgModule({
  declarations: [
    Demo1Component,
    Demo2Component,
    Demo3Component,
    ChildComponent,
    Demo4Component,
    Demo5Component,
    Demo6Component,
    AdminComponent,
    UserComponent,
    Demo7Component,
    Demo8Component
  ],
  exports: [
    Demo3Component
  ],
  imports: [
    CommonModule,
    DemoRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class DemoModule { }
