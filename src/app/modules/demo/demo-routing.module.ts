import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {Demo1Component} from "./components/demo1/demo1.component";
import {Demo2Component} from "./components/demo2/demo2.component";
import {Demo3Component} from "./components/demo3/demo3.component";
import {Demo4Component} from "./components/demo4/demo4.component";
import {Demo5Component} from "./components/demo5/demo5.component";
import {AdminComponent} from "./components/demo6/admin/admin.component";
import {UserComponent} from "./components/demo6/user/user.component";
import {AdminGuard} from "./components/demo6/guards/admin.guard";
import {AuthGuard} from "./components/demo6/guards/auth.guard";
import {Demo6Component} from "./components/demo6/demo6.component";
import {ItemResolver} from "./components/demo6/resolver/item-resolver.service";
import {Demo7Component} from "./components/demo7/demo7.component";
import {Demo8Component} from "./components/demo8/demo8.component";

const routes: Routes = [
  { path: 'demo1', component: Demo1Component },
  { path: 'demo2', component: Demo2Component },
  { path: 'demo3', component: Demo3Component },
  { path: 'demo4', component: Demo4Component },
  { path: 'demo5', component: Demo5Component },
  { path: 'demouser', component: UserComponent },
  { path: 'demoadmin', component: AdminComponent, canActivate: [AdminGuard] },
  { path: 'demo6', canActivateChild: [AuthGuard], children: [
    { path: 'admin', component: AdminComponent, canActivate: [AdminGuard] },
    { path: 'user', component: UserComponent },
  ]},
  { path: 'demo7/:id', component: Demo6Component},
  { path: 'demo8/:id', resolve: { item: ItemResolver }, component: Demo6Component},
  { path: 'demo9', resolve: { item: ItemResolver }, component: Demo7Component},
  { path: 'demo10', resolve: { item: ItemResolver }, component: Demo8Component},
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class DemoRoutingModule { }
