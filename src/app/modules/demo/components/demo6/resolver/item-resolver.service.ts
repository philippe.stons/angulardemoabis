import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of } from 'rxjs';
import {ItemService} from "../../../../exercices/components/exercice3/service/item.service";
import {Item} from "../../demo4/service/example.service";

@Injectable({
  providedIn: 'root'
})
export class ItemResolver implements Resolve<Item> {
  constructor(
    private itemService: ItemService
  ) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Item {
    let id = route.params['id'];
    let item = this.itemService.getOneById(id);
    console.log(id, item);
    return item;
  }
}
