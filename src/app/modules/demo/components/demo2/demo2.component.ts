import { Component, OnInit } from '@angular/core';

class DemoData
{
  public id: number;
  public text: string;
}

@Component({
  selector: 'app-demo2',
  templateUrl: './demo2.component.html',
  styleUrls: ['./demo2.component.css']
})
export class Demo2Component implements OnInit {
  isOn: boolean;
  values = ['Riri', 'Fifi', 'Loulou'];
  switchVal = 121;
  asdf: Array<DemoData>;

  constructor() { }

  ngOnInit(): void {
    this.asdf = new Array<DemoData>();
    this.asdf.push({ text: 'Hello', id: 1 })
    this.asdf.push({ text: 'Asdasdas', id: 2 })

    this.asdf = [
      { text: 'Hello', id: 1 },
      { text: 'Hello', id: 1 },
      { text: 'Hello', id: 1 },
      { text: 'Hello', id: 1 },
    ];

  }

  toggle()
  {
    this.isOn = !this.isOn;
  }
}
