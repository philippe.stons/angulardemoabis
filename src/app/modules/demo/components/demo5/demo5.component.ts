import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-demo5',
  templateUrl: './demo5.component.html',
  styleUrls: ['./demo5.component.css']
})
export class Demo5Component implements OnInit {
  formGrp: FormGroup;

  constructor(
    private fb: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.formGrp = this.fb.group({
      login: [null, [Validators.required]],
      password: [null, [Validators.required]],
      confirmPassword: [null, [Validators.required]],
    }, { validators: this.validatePasword() });
  }

  validatePasword()
  {
    return (grp: FormGroup) =>
    {
      const password = grp.get('password');
      const login = grp.get('login');
      const confirm = grp.get('confirmPassword');
      password.setErrors(null);
      confirm.setErrors(null);

      // console.log(password, confirm);
      // console.log(login.errors);
      // console.log(password.value, confirm.value, password.value === confirm.value)
      if(password.value !== confirm.value)
      {
        password.setErrors({ match: 'Password does not match' });
        confirm.setErrors({ match: 'Password does not match' });
      }

      return null;
    }
  }

  getFieldErrors(fieldName: string): Array<any>
  {
    let errors = [];
    for(let err in this.formGrp.get(fieldName).errors)
    {
      errors.push(err);
    }

    return errors;
  }

  logChange()
  {
    console.log(this.formGrp.get('login').value);
  }

  submit()
  {
    console.log(this.formGrp);
    if(!this.formGrp.valid) {
      return;
    }

    console.log("VALID!!!")
  }
}
