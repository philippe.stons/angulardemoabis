import { Component, OnInit } from '@angular/core';
import {ExampleService, Item} from "./service/example.service";

@Component({
  selector: 'app-demo4',
  templateUrl: './demo4.component.html',
  styleUrls: ['./demo4.component.css']
})
export class Demo4Component implements OnInit {
  itemList: Array<Item>;

  constructor(
    private exampleService: ExampleService
  ) { }

  ngOnInit(): void {
    this.itemList = this.exampleService.getAll();
  }

  buttonTriggerService() {
    this.exampleService.hello();
  }

}
