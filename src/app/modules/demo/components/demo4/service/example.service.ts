import { Injectable } from '@angular/core';

export class Item {
  id: number;
  name: string;
  quantity: number;
}

@Injectable({
  providedIn: 'root'
})
export class ExampleService {
  itemList: Array<Item>;

  constructor() {
    console.log("CREATE SERVICE !!!")
    this.itemList = [
      { id: 1, name: 'Mouse', quantity: 5 },
      { id: 2, name: 'headset', quantity: 5 },
      { id: 3, name: 'COmputer', quantity: 5 },
      { id: 4, name: 'gloves', quantity: 5 },
    ]
  }

  public hello(){
    console.log("Hello !!!");
  }

  getAll()
  {
    return this.itemList;
  }

  getOneById(id: number)
  {
    let subList = this.itemList.filter((item) => item.id === id);

    return subList.length === 1 ? subList[0] : null;
  }

  insert(item: Item)
  {
    this.itemList.push(item);
  }

  delete(item: Item)
  {
    let index = this.itemList.indexOf(item);
    this.itemList.splice(index, 1);
  }
}
