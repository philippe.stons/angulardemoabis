import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit, OnDestroy {
  @Input() value: string;

  @Output() event: EventEmitter<string>;

  counter: number;
  eventString: string;

  constructor() {
    this.event = new EventEmitter<string>();
    this.eventString = "";
  }

  ngOnInit(): void {
    this.counter = 0;
  }

  ngOnDestroy() {
    console.log("DESTROYED")
  }

  triggerEvent()
  {
    this.event.emit(this.eventString + this.counter++);
  }
}
