import {Component, OnDestroy, OnInit} from '@angular/core';
import {map, Subscription} from "rxjs";
import {RxjsExampleService} from "./service/rxjs-example.service";

@Component({
  selector: 'app-demo7',
  templateUrl: './demo7.component.html',
  styleUrls: ['./demo7.component.css']
})
export class Demo7Component implements OnInit, OnDestroy {
  subscription1: Subscription;
  subscription2: Subscription;
  timer: any;
  nbr: number;

  constructor(
    private rxjsExample: RxjsExampleService,
  ) { }

  ngOnInit(): void {
    this.nbr = 0;

    this.subscription1 = this.rxjsExample.getSubject().subscribe((val) =>
    {
      console.log(val);
    });

    this.subscription2 = this.rxjsExample.getSubject()
      .pipe(map((val) => 2 * val))
      .subscribe((val) =>
    {
      console.log(val);
    });

    this.timer = setInterval(() => {
      this.rxjsExample.triggerSubject(this.nbr++);
      if(this.nbr === 100)
      {
        clearInterval(this.timer);
      }
    }, 1000)
  }

  ngOnDestroy(): void {
    this.subscription1.unsubscribe();
    this.subscription2.unsubscribe();
    clearInterval(this.timer);
  }
}
