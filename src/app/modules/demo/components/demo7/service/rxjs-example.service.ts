import { Injectable } from '@angular/core';
import {Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class RxjsExampleService {
  private subject: Subject<number>;

  constructor() {
    this.subject = new Subject<number>();
  }

  getSubject()
  {
    return this.subject;
  }

  triggerSubject(value: number)
  {
    this.subject.next(value);
  }
}
