import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo1',
  templateUrl: './demo1.component.html',
  styleUrls: ['./demo1.component.css']
})
export class Demo1Component implements OnInit {
  time: number;
  timer: any;
  started: boolean = false;
  toggleButtons: boolean;
  currentDate = new Date();

  constructor() { }

  ngOnInit(): void {
    this.time = 0;
  }

  toggleBtns()
  {
    this.toggleButtons = !this.toggleButtons;
  }

  toggleTimer()
  {
    let str = `${this.currentDate} ${this.time}`
    if(!this.started)
    {
      this.startTime();
    } else {
      this.stopTimer();
    }
  }

  private startTime()
  {
      this.timer = setInterval(() => {
        this.time++
      }, 10);
      this.started = true;
  }

  private stopTimer()
  {
    this.started = false;
    clearTimeout(this.timer);
  }
}
