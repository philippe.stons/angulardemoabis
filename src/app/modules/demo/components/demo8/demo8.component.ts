import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {AgifyService} from "./service/agify.service";
import {Person} from "./model/person.model";

@Component({
  selector: 'app-demo8',
  templateUrl: './demo8.component.html',
  styleUrls: ['./demo8.component.css']
})
export class Demo8Component implements OnInit {
  searchGrp: FormGroup;

  constructor(
    private fb: FormBuilder,
    private agifyService: AgifyService,
  ) { }

  ngOnInit(): void {
    this.searchGrp = this.fb.group({
      name: [null]
    });
  }

  submit()
  {
    this.agifyService.getNameInfo(this.searchGrp.controls['name'].value)
      .subscribe((info: Person) =>
      {
        console.log(info.name);
        console.log(info.age);
      });
  }
}
