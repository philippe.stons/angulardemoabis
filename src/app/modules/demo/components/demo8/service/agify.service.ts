import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Person} from "../model/person.model";

@Injectable({
  providedIn: 'root'
})
export class AgifyService {

  constructor(
    private http: HttpClient,
  ) { }

  public getNameInfo(name: string)
  {
    return this.http.get<Person>(`https://api.agify.io?name=${name}`);
  }
}
