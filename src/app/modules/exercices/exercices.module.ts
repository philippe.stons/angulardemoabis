import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Exercice1Component } from './components/exercice1/exercice1.component';
import {ExercicesRoutingModule} from "./exercices-routing.module";
import { Exercice2Component } from './components/exercice2/exercice2.component';
import {SharedModule} from "../shared/shared.module";
import { Exercice3Component } from './components/exercice3/exercice3.component';
import { ListComponent } from './components/exercice3/list/list.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { DetailComponent } from './components/exercice4/detail/detail.component';
import { NewComponent } from './components/exercice4/new/new.component';
import { UpdateComponent } from './components/exercice4/update/update.component';
import {BrowserModule} from "@angular/platform-browser";
import {FanListComponent} from "./components/exercice4/list/fan-list.component";

@NgModule({
  declarations: [
    Exercice1Component,
    Exercice2Component,
    Exercice3Component,
    ListComponent,
    DetailComponent,
    NewComponent,
    UpdateComponent,
    FanListComponent,
  ],
  imports: [
    CommonModule,
    ExercicesRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class ExercicesModule { }
