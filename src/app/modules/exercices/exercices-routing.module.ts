import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {Exercice1Component} from "./components/exercice1/exercice1.component";
import {Exercice2Component} from "./components/exercice2/exercice2.component";
import {Exercice3Component} from "./components/exercice3/exercice3.component";
import {NewComponent} from "./components/exercice4/new/new.component";
import {FanListComponent} from "./components/exercice4/list/fan-list.component";
import {DetailComponent} from "./components/exercice4/detail/detail.component";

const routes: Routes = [
  { path: 'ex1', component: Exercice1Component },
  { path: 'ex2', component: Exercice2Component },
  { path: 'ex3', component: Exercice3Component },
  { path: 'ex4/add', component: NewComponent },
  { path: 'ex4/edit/:id', component: NewComponent },
  { path: 'ex4/detail/:id', component: DetailComponent },
  { path: 'ex4/list', component: FanListComponent },
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class ExercicesRoutingModule { }
