export class Fan {
  name: string;
  birthDate: Date;
  series: string[];
}
