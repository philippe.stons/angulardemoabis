import { Component, OnInit } from '@angular/core';
import {FanService} from "../services/fan.service";
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Fan} from "../models/fan.model";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.css']
})
export class NewComponent implements OnInit {
  entityForm: FormGroup;
  edit: boolean = false;
  fan: Fan;
  id: number;

  constructor(
    private fanService: FanService,
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.params['id']

    if(!isNaN(id))
    {
      this.id = id;
      this.fan = this.fanService.getOneById(id);
      this.edit = true;
    }

    this.entityForm = this.fb.group({
      name: [this.fan ? this.fan.name : null, Validators.required],
      birthDate: [this.fan ? this.fan.birthDate : null, Validators.required],
      series: this.fb.array([]),
    });

    if(this.edit)
    {
      for(const serie of this.fan.series)
      {
        this.addSerie(serie);
      }
    }
  }

  validateDate()
  {
    return (ctrl: FormControl) =>
    {
      const today = new Date();
      const birthYear = new Date(ctrl.value);
      let birthDay = new Date();
      birthDay.setDate(birthYear.getDate());
      birthDay.setMonth(birthYear.getMonth());

      if(today.getFullYear() - birthYear.getFullYear() < 13)
      {
        ctrl.setErrors({ invalidYear: 'You must be older than 13 winters old!' });
      }

      return null;
    }
  }

  getSeries(): FormArray
  {
    return this.entityForm.get('series') as FormArray;
  }

  addSerie(value: string = null){
    this.getSeries().push(new FormControl(value, Validators.required));
  }

  removeSerie(id: number)
  {
    this.getSeries().removeAt(id);
  }

  submitForm()
  {
    if(this.entityForm.invalid){
      return;
    }

    let newFan = new Fan();
    newFan.name = this.entityForm.get('name').value;
    newFan.birthDate = this.entityForm.get('birthDate').value;
    newFan.series = this.entityForm.get('series').value ? this.entityForm.get('series').value : [];

    this.fanService.save(newFan, this.edit ? this.id : -1);
    this.entityForm.reset();
    this.router.navigate(['/exercices/ex4/list']);
  }
}
