import { Component, OnInit } from '@angular/core';
import {FanService} from "../services/fan.service";
import {Fan} from "../models/fan.model";
import {Router} from "@angular/router";
import {Logger} from "../../../../../servicces/logger.service";

@Component({
  selector: 'app-list',
  templateUrl: './fan-list.component.html',
  styleUrls: ['./fan-list.component.css']
})
export class FanListComponent implements OnInit {
  fanList: Fan[];

  constructor(
    private fanService: FanService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.fanList = this.fanService.getAll();
  }

  edit(id: number)
  {
    this.router.navigate(['/exercices/ex4/edit', id]);
  }

  detail(id: number)
  {
    this.router.navigate(['/exercices/ex4/detail', id]);
  }
}
