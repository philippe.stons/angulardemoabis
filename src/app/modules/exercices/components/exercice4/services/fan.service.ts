import { Injectable } from '@angular/core';
import {Fan} from "../models/fan.model";

@Injectable({
  providedIn: 'root'
})
export class FanService {
  fans: Fan[];

  constructor() {
    this.fans = [];
  }

  getAll()
  {
    return this.fans;
  }

  getOneById(id: number)
  {
    return this.fans[id];
  }

  save(fan: Fan, id: number = -1)
  {
    if(id == -1)
    {
      this.fans.push(fan);
    } else
    {
      this.fans[id] = fan;
    }

    return fan;
  }

  delete(id: number) {
    this.fans.splice(id, 1);
  }
}
