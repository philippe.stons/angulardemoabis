import { Component, OnInit } from '@angular/core';
import {FanService} from "../services/fan.service";
import {Fan} from "../models/fan.model";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  fan: Fan;
  id: number;

  constructor(
    private fanService: FanService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.params['id']

    if(!isNaN(id))
    {
      this.id = id;
      this.fan = this.fanService.getOneById(id);
    }
  }

  edit()
  {
    this.router.navigate(['/exercices/ex4/edit', this.id])
  }
}
