import {Component, OnDestroy, OnInit} from '@angular/core';

@Component({
  selector: 'app-exercice2',
  templateUrl: './exercice2.component.html',
  styleUrls: ['./exercice2.component.css']
})
export class Exercice2Component implements OnInit, OnDestroy {
  timer: any;
  time: number;
  isRunning: boolean;

  constructor() { }

  ngOnInit(): void {
    this.time = 0;
    this.isRunning = false;
  }

  ngOnDestroy(): void {
    clearInterval(this.timer);
  }

  toggleTimer()
  {
    if(this.isRunning)
    {
      this.isRunning = false;
      clearInterval(this.timer);
    } else
    {
      this.timer = setInterval(() =>
      {
        this.time++;
      }, 1000)
      this.isRunning = true;
    }
  }

  resetTimer()
  {
    this.time = 0;
  }

  get isStarted()
  {
    return this.time != 0;
  }
}
