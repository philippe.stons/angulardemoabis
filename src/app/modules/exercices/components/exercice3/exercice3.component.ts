import { Component, OnInit } from '@angular/core';
import {ItemService} from "./service/item.service";
import {Item} from "../../../demo/components/demo4/service/example.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-exercice3',
  templateUrl: './exercice3.component.html',
  styleUrls: ['./exercice3.component.css']
})
export class Exercice3Component implements OnInit {
  itemName: string;
  itemQty: number;
  itemName2: string;
  itemList: Array<Item>
  itemList2: Array<Item>
  entityForm: FormGroup;

  constructor(
    private itemService: ItemService,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.itemList = this.itemService.getAll();
    this.itemList2 = [];

    this.entityForm = this.fb.group({
      name: [null, Validators.required],
      quantity: [null, Validators.required],
    });
  }

  addItem()
  {
    if(!this.entityForm.valid)
    {
      return;
    }

    let item = new Item();
    item.id = Math.random();
    item.quantity = this.entityForm.get('quantity').value;
    item.name = this.entityForm.get('name').value;
    this.itemList.push(item);
    this.itemName = "";
  }

  addItem2()
  {
    this.itemList2.push({ name: this.itemName2, quantity: 0, id: Math.random() });
    this.itemName2 = "";
  }

  deleteItem(index: number)
  {
    this.itemList.splice(index, 1);
  }

  deleteItem2(index: number)
  {
    this.itemList2.splice(index, 1);
  }
}
