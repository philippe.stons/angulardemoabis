import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Item} from "../../../../demo/components/demo4/service/example.service";
import {ItemService} from "../service/item.service";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  @Input() itemList: Array<Item>;
  @Output() deleteEvent: EventEmitter<number> = new EventEmitter<number>();

  constructor(private itemService: ItemService) { }

  ngOnInit(): void {
  }

  deleteItem(item: Item)
  {
    // this.deleteEvent.emit(index);
    this.itemService.delete(item)
  }

}
