import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'secToMin'
})
export class SecToMinPipe implements PipeTransform {

  transform(value: number, ...args: unknown[]): unknown {
    const min = Math.floor(value / 60);
    const sec = value % 60;
    return `${min} min ${sec} sec`;
  }

}
