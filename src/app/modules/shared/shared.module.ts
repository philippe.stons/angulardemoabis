import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CsToMsPipe } from './pipes/cs-to-ms.pipe';
import { SecToMinPipe } from './pipes/sec-to-min.pipe';



@NgModule({
  declarations: [
    CsToMsPipe,
    SecToMinPipe,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    CsToMsPipe,
    SecToMinPipe,
  ]
})
export class SharedModule { }
